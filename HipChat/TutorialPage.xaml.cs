﻿using HipChat.Libs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HipChat
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class TutorialPage : Page
    {
        public TutorialPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendView("tutorial");
        }

        private void UpdateActivePageDot()
        {
            foreach (Ellipse ellipse in pageControl.Children)
            {
                ellipse.Fill = new SolidColorBrush(Color.FromArgb(255,82,94,112));
            }

            ((Ellipse)pageControl.Children.ElementAt(pages.SelectedIndex)).Fill = new SolidColorBrush(Colors.White);
        }

        private void Proceed_Tapped(object sender, RoutedEventArgs e)
        {
            Account.Current.HasSeenTutorial = true;

            if (API.Current.IsConnected)
            {
                this.Frame.Navigate(typeof(MainPage));
            }
            else
            {
                this.Frame.Navigate(typeof(Login));
            }
        }

        private void pages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateActivePageDot();
        }

        private void OpenWebsite_Tapped(object sender, TappedRoutedEventArgs e)
        {
            (Application.Current as App).OpenHipChatXmppPage();
        }
    }
}
